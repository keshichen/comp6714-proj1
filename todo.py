import torch
from config import config
import torch.nn.functional as F

_config = config()


def evaluate(golden_list, predict_list):
    pass;


def new_LSTMCell(input, hidden, w_ih, w_hh, b_ih=None, b_hh=None):
    hx, cx = hidden
    gates = F.linear(input, w_ih, b_ih) + F.linear(hx, w_hh, b_hh)

    ingate, forgetgate, cellgate, outgate = gates.chunk(4, 1)

    #ingate = F.sigmoid(ingate)
    ingate = 1 - forgetgate
    #print(1-forgetgate)
    forgetgate = F.sigmoid(forgetgate)
    cellgate = F.tanh(cellgate)
    outgate = F.sigmoid(outgate)

    cy = (forgetgate * cx) + (ingate * cellgate)
    hy = outgate * F.tanh(cy)

    return hy, cy
    #pass;


def get_char_sequence(model, batch_char_index_matrices, batch_word_len_lists):
    pass;

